#include <stdio.h>

int main()
{
	char city_name[15] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	float distance;
	float weight_total = 0;
	char count_products = 1;

	printf ("Ingrese el nombre de la cuidad de destino:\n");
	scanf("%s", city_name);
	printf ("Ingrese la distancia de la cuidad de destino(Km):\n");
	scanf("%f", &distance);

	float weight_product;
	
	if(distance<=500.0){ //Despacho camion
		for(int i=0;i<5;i++){ //Una caja(5 productos)
			for(;;){
				printf("Ingrese el peso del producto(Kg) (%d): ", i+1);
				scanf("%f", &weight_product);
				if((weight_product>0.5)&&(weight_product<3.0)){
					weight_total = weight_total + weight_product;
					count_products++;
					break;
				}
				printf("Error: El producto debe pesar entre 0.5 y 3kg\n");
			}
		}
	}else{ //Despacho avion
		for(int i;i<10;i++){ //Un paquete(10 productos - 15kg max)
			printf("Ingrese el peso del producto (Kg)(%d): ", i+1);
			scanf("%f", &weight_product);
			if(weight_total + weight_product>15) break; //Limite de peso 15kg
			weight_total = weight_total + weight_product;
			count_products++;
		}
	}
	
	printf("\n");
	printf(" Ciudad de destino: %s\n", city_name);
	printf(" Distancia del viaje: %f\n", distance);
	printf(" Cantidad de productos: %d\n", count_products);
	printf(" Peso de los productos: %f\n", weight_total);
	return 0;
}
